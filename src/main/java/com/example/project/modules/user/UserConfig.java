package com.example.project.modules.user;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.util.*;

@Configuration
public class UserConfig {

    @Bean
    CommandLineRunner commandLineRunner(UserRepository userRepository) {

        return args -> {

            // User kenny1 = new User(
            // "kene1@gmail.com",
            // "Kenechukwu",
            // "Arionye");

            User kenny1 = new User();
            kenny1.setEmail("kene112121212@gmail.com");
            kenny1.setFirstName("Kenechukwu");
            kenny1.setLastName("Arionye");

            // User kenny2 = new User(
            // "kene2@gmail.com",
            // "Kenechukwu",
            // "Arionye");

            User kenny2 = new User();
            kenny2.setEmail("kene22114141415@gmail.com");
            kenny2.setFirstName("Kenechukwu");
            kenny2.setLastName("Arionye");

            userRepository.saveAll(
                    List.of(kenny1, kenny2));
        };
    }

}
