package com.example.project.modules.user;

import java.util.*;

// import org.springdoc.core.annotations.ParameterObject;
// import org.springdoc.core.converters.models.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// import io.swagger.v3.oas.annotations.Parameter;
// import io.swagger.v3.oas.annotations.media.Schema;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;

// import io.swagger.v3.oas.annotations.parameters.RequestBody;

@RestController
@RequestMapping("api/v1/user")
public class UserController {

    @Autowired
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping()
    public List<User> getAllUser() {
        return userService.getAllUser();
    }

    @PostMapping()
    public void addNewUser(@RequestBody User userDto) {
        userService.addNewUser(userDto);
    }

    @DeleteMapping(path = "{userId}")
    public void deleteUser(@PathVariable("userId") Long userId) {
        userService.deleteUser(userId);
    }

    @PutMapping("{userId}")
    public void updateUser(
            @PathVariable("userId") Long userId,
            @RequestBody(required = true) String firstName,
            @RequestBody(required = true) String email,
            @RequestBody(required = true) String lastName) {
        userService.updateUser(userId, firstName, lastName, email);
    }

    // @GetMapping("{userId}")
    // public String getSingleUser(@PathVariable int userId) {
    // System.out.println(userId);
    // return userService.getAllUser();
    // }

}
