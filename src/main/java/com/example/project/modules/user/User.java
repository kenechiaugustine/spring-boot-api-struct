package com.example.project.modules.user;

import lombok.Data;
import lombok.Getter;

import java.time.LocalDateTime;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.example.project.common.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;

// import lombok.Setter;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import jakarta.persistence.*;

// @Getter
// @Setter
// @AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "users")
public class User extends BaseEntity {

    @Id
    @SequenceGenerator(name = "users_sequence", sequenceName = "users_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_sequence")
    private Long id;

    private String email;
    private String firstName;
    private String lastName;

    // Constructors ->
    // public User() {

    // }

    User(String email, String firstName, String lastName) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    // User(Long id, String email, String firstName, String lastName) {
    // this.email = email;
    // this.firstName = firstName;
    // this.lastName = lastName;
    // }

    // Getter and Setter ->
    // public Long getId() {
    // return id;
    // }

    // public void setId(Long id) {
    // this.id = id;
    // }

    // @JsonIgnore
    // public String getEmail() {
    // return email;
    // }

    // public void setEmail(String email) {
    // this.email = email;
    // }

    // public String getFirstName() {
    // return firstName;
    // }

    // public void setFirstName(String firstName) {
    // this.firstName = firstName;
    // }

    // public String getLastName() {
    // return lastName;
    // }

    // public void setLastName(String lastName) {
    // this.lastName = lastName;
    // }

    // @Override
    // public String toString() {
    // return "User{" +
    // "id=" + id +
    // ", email=" + email +
    // ", firstName=" + firstName +
    // ", lastName=" + lastName +
    // "}";
    // }
}
