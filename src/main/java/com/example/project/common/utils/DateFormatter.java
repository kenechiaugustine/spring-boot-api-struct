package com.example.project.common.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateFormatter {

    // Utility function to format a given LocalDate to a string
    public static String formatLocalDate(LocalDate date, String pattern) {
        if (date == null || pattern == null || pattern.isEmpty()) {
            throw new IllegalArgumentException("Date and pattern must not be null or empty");
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return date.format(formatter);
    }
}