package com.example.project.common.config;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.servers.Server;

@Configuration
public class SwaggerConfig {

    @Bean
    public OpenAPI defineOpenApi() {
        Server server = new Server();
        server.setUrl("http://localhost:8081");
        server.setDescription("Development");

        Contact myContact = new Contact();
        myContact.setName("Arionye Kenechukwu");
        myContact.setEmail("kenechukwu.arionye@gmail.com");

        Info information = new Info()
                .title("Project API Documentation")
                .version("1.0")
                .description("This API exposes endpoints for ....")
                .contact(myContact);
        return new OpenAPI().info(information).servers(List.of(server));
    }
}

// @Configuration
// public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
// @Override
//     public void configure(WebSecurity web) throws Exception {
//         web.ignoring().antMatchers("/swagger-ui/**", "/v3/api-docs/**");
//     }
// }