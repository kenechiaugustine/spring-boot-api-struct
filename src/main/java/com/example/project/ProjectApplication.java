package com.example.project;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.filter.ForwardedHeaderFilter;

@SpringBootApplication
public class ProjectApplication {

	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(ProjectApplication.class);
		application.setBannerMode(Banner.Mode.OFF);
		application.run(args);
	}

	// @Bean
	// ForwardedHeaderFilter forwardedHeaderFilter() {
	// 	return new ForwardedHeaderFilter();
	// }

}
